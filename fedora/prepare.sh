#!/bin/sh

set -eu

dnf install -y bubblewrap bzr findutils fuse git lzip make patch \
               python3-gobject python3-psutil ostree which

# BuildStream dependencies
pip3 install blessings Click jinja2 MarkupSafe pluginbase ruamel.yaml

# Test suite dependencies
pip3 install apipkg coverage==4.4.0 execnet pep8 pytest pytest-cache pytest-cov \
             pytest-datafiles pytest-env pytest-pep8 pytest-xdist

# Once OSTree is installed, DNF's 'protected package' feature will refuse
# to remove it again due to systemd-udev depending on it, and systemd-udev
# being somehow protected.
#
# The BuildStream test suite expects to be able to `dnf erase ostree` to
# test that the fallback Unix backend doesn't try to use OSTree anywhere
# so we need it to be possible.
#
# This change makes the container vulnerable to running `dnf erase dnf`
# but hey, it's a container, you get to keep the pieces.
echo 'protected_packages=[]' >> /etc/dnf/dnf.conf
