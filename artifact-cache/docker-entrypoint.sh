#!/bin/bash

set -eu

CONFIG_VOLUME="/home/artifacts/.config"
DATA_VOLUME="/data"

PULL_URL="http://localhost:8080/artifacts/"

# Set up the OSTree repo.
#
# It's safe to run the `init` command on an existing repo.
mkdir -p "${DATA_VOLUME}/artifacts"
ostree init --mode=archive-z2 --repo="${DATA_VOLUME}/artifacts"

# Set up nginx, to serve artifacts over HTTP protocol.
#
# The 'server' directive is stored in a seperate file. This allows the user
# to replace it with an SSL-enabled server if desired.
mkdir -p "${CONFIG_VOLUME}/nginx/server.d"

cat > "${CONFIG_VOLUME}/nginx/nginx.conf" <<"EOF"
pid /var/run/nginx/nginx.pid;

events {
    worker_connections 1024;
}

http {
    access_log /var/run/nginx/access.log;

    autoindex on;

    include server.d/*.conf;
}
EOF

if [ ! -e "${CONFIG_VOLUME}/nginx/server.d/default.conf" ]; then
cat > "${CONFIG_VOLUME}/nginx/server.d/default.conf" <<EOF
server {
    listen 8080;

    location / {
        root "${DATA_VOLUME}";
    }
}
EOF
fi

nginx -t -c "${CONFIG_VOLUME}/nginx/nginx.conf"

# Set up sshd, to receive artifacts over SSH protocol.
mkdir -p "${CONFIG_VOLUME}/ssh"

cat > "${CONFIG_VOLUME}/ssh/sshd_config" <<EOF
AuthorizedKeysFile "${CONFIG_VOLUME}/ssh/authorized_keys"
HostKey "${CONFIG_VOLUME}/ssh/ssh_host_rsa_key"
Port 22200

AllowUsers artifacts
Match user artifacts
  PasswordAuthentication no
  ForceCommand bst-artifact-receive --verbose --pull-url ${PULL_URL} ${DATA_VOLUME}/artifacts
EOF

if [ ! -e "${CONFIG_VOLUME}/ssh/ssh_host_rsa_key" ]; then
    ssh-keygen -t rsa -f "${CONFIG_VOLUME}/ssh/ssh_host_rsa_key" -N ''
fi

# Start your daemons!
/usr/sbin/nginx -c "${CONFIG_VOLUME}/nginx/nginx.conf"
NGINX_PID=$(cat /var/run/nginx/nginx.pid)
echo "HTTP server (nginx) running with process ID ${NGINX_PID} on port 8080"

/usr/sbin/sshd -f ${CONFIG_VOLUME}/ssh/sshd_config -D &
SSHD_PID=$!
echo "SSH daemon (sshd) running with process ID ${SSHD_PID} on port 22200"

trap "set +e; kill $SSHD_PID 2>/dev/null; kill $NGINX_PID 2>/dev/null" ERR EXIT TERM

# Give daemons little time to fail, if they're going to
sleep 2

process_running() {
    ps -p $1 > /dev/null
}

# Main loop.
while true ; do
    if ! process_running $NGINX_PID; then
        echo >&2 "nginx process died"
        exit 1
    fi

    if ! process_running $SSHD_PID; then
        echo >&2 "sshd process died"
        exit 1
    fi

    # We need to update the summary file regularly.
    ostree --repo="${DATA_VOLUME}/artifacts" summary --update
    sleep 60
done
